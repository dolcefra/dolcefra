<?php
  $nom = "Dupré";
  $prenom = "Gérard";
  $style = 1;
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Bienvenue dans PHP</title>
    <link rel="stylesheet" href="style<?php echo $style; ?>.css"> 
  </head>

  <body>
    <h1>Premier Test PHP</h1>

    <!-- version inclusion php minimale --> 
    <p>Bonjour je m'appelle
        <?php
            echo $prenom, " ", $nom;	  
         ?>.   
      Bienvenue! </p>

       <!-- version tout le paragraphe en guillement -->	
      <?php
            echo "<p>Bonjour je m'appelle $prenom $nom;	    Bienvenue! </p>";
      ?>  
     
      <!-- version tout le paragraphe en quote -->	
      <?php
            echo '<p> Bonjour je m\'appelle',$prenom,
	    ' ', $nom,';	    Bienvenue! </p>';
      ?>  

   Date:  <?php echo date("d/m/Y"); ?>, Heure  <?php echo date("H:i"); ?>

  
  </body>
</html>
