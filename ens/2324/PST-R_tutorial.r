########################
########################
## Probability and statistics BIE-PST
## Using the R software

# launch using Rgui
# the programs are usually written and saved as .r scripts
# single lines or selections from the script can be run by pressing ctrl+r
# commands and their results are executed in the console window

#######################
## Clear all allocated variables

rm(list=ls())

#######################
## R works as a calculator

3*8^2+10/log(5)+exp(-20)+factorial(6)

#######################
## ALLOCATING AND CALLING OF VARIABLES

# !all variable and object names are CASE SENSITIVE! 

# R uses a vector structure
# vector of length 1 containing the number 3
x=3				 # also x<-3
# vector of three numbers
y=c(1,5,9)
# joining of x and y into one vector of length 4
z=c(x,y)

#calling the values
x
y
z
# vector consisting of ten times zero 
x=rep(0,10)
x
#vector consisting of numbers 1 to 10
x=1:10
x
#calling the fourth element of x
x[4]
#calling the fourth and eighth element of x
x[c(4,8)]

#matrix 2x3 containing the numbers 1,2,3,4,5,6
X=matrix(1:6,2,3)
X
X[1,2]

# logical variables
a=T			# also a=TRUE
b=F			# also a=False
			# logical variables can be worked with as with zeroes and ones (multiplying, adding,...)
			# "and" and "or" denoted by & and | 
a*a

# character and factor variables
z=c(rep("M",5),rep("F",5))
z

class(z)		## class of an object
z=as.factor(z)	## turning an object into factor (variable with several possible specified levels)
z
class(z)
class(x)
class(a)

# data frame - tabulated data
x=1:10;y=rep(0,10);

dat=data.frame(x,y,z)
dat
names(dat)=c("id","salary","gender")
dat
dat[1,]	
dat$gender	
dat$gender[1:6]

#######################
## USING BUILT-IN COMPUTATIONAL FUNCTIONS

# the argument of the function is a vector of numbers
sum(x)		#sum
mean(x)		#sample mean
var(x)		#sample variance S^2
sd(x)			#sqrt(S^2)
length(x)		#returns the length of the vector x


# vector algebra:
x^2		#applies square to each element
log(x)	#takes natural logarithm of each element
x-5		#substracts 5 from each element
x-mean(x)   #substracts the mean from each element x
x<=5		#returns a vector of the same length of x, containing TRUE or FALSE

sum(x<=5)	#returns the number of cases, when x<=5

#example: computation of the sample variance S^2 (same as var(x) ):
sum( (x-mean(x))^2 )/(length(x)-1)

## CYCLES

for(i in 1:10){
print(x[i])
}

for(i in 10:1){
print(x[i])
}

z=rep(0,10)
for(i in 1:10){z[i]=mean(x[-i])}
z

i=1;z=0
while(z<=20){
z=z+log(i)
i=i+1
}
z

##CONDITIONS

if(x[1]==6){z=4}else{z=6}	#  < > <= >= !=

for(i in 1:10){
 if((x[i]<=5)&sum(x[-i]<=20)){
  print("Hello world")
 }
}

####################
## STATISTICAL FUNCTIONS

## consider the example with beer volumes from last time:
x=c(0.510,0.462,0.491,0.466,0.451,0.503,0.475,0.487,0.512,0.505)

t.test(x,mu=0.5,alternative="less")		##t-test of the mean value of a distribution with desired alternative
t.test(x,mu=0.5)					##default is testing against the two-sided alternative mu<>mu0

# hypothesis that the true mean is mu is rejected on level 0.05, if the p-value is less than 0.05
# we also obtain the pointwise and interval estimates for mu

# interval estimates can be obtained also as
n=length(x)

c(
mean(x)-qt(0.975,df=n-1)*sd(x)/sqrt(n),
mean(x)+qt(0.975,df=n-1)*sd(x)/sqrt(n)
)

#qt(0.975,df=n-1) is the quantile (inverse of the distribution function) of the t-distribution with n-1 degrees of freedom at point 1-alpha/2 for alpha=0.05

#approximate confidence interval can be obtained by using the CLT and the quantiles of standardised normal distribution phi^-1:
c(mean(x)-qnorm(0.975)*sd(x)/sqrt(n),mean(x)+qnorm(0.975)*sd(x)/sqrt(n))

## CHARACTERISTICS OF THE DISTRIBUTIONS

?qnorm ## calling help
## we can call the density/probability values, distribution function, quantiles and random numbers
?qexp()

## PAIRED T-TEST
## used when comparing the expectations of (possibly dependent) pairs of random variables
n=10
height_father=rnorm(n,mean=175,sd=5)
height_son=rep(0,n)
for(i in 1:n){height_son[i]   =rnorm(1,mean=height_father[i]+5,sd=5) }
t.test(height_father,height_son)

## TWO SAMPLE T-TEST
## used when comparing the expectations between two independent random samples

n=10;m=20
height_usa  =rnorm(m,mean=180,sd=5)
height_china=rnorm(n,mean=165,sd=5)
t.test(height_usa,height_china)

	 
####################
## SIMULATIONS

## setting the initial simulation value (useful when we want to repeat the simulation in future)
set.seed(12345678)

# generate 50 values from N(100,15^2)
n=50
x=rnorm(n,mean=100,sd=15)		# also rnorm(n,100,15^2)
x

# generate 50 values from exponential distribution with lambda=5
x=rexp(n,rate=5)

## Exercise 1: using simulations, we want to find such a value, that in 95% cases won't be exceeded by a sum of 50 independent random variables from Exp(5).
## We generate N times 50 numbers from Exp(5) and mark their sum each time.
## We take the 95%-sample quantile, meaning that when we order the generated sums, we take that which is on the 95%-th position

N=1000
z=rep(0,N)
for(i in 1:N){
 z[i]=sum(rexp(50,5))
}
quantile(z,0.95)
sort(z)[0.95*N]

## we got 12.5, for a different seed the result could be different

## Exercise 2: we want to compute the probability, that the sum of 50 independent values from Exp(5) won't be greater than 13
## We simply compute the number of cases out of the N generated sums, in which it occured.
## We produce a vector of TRUE/FALSE values z<=13, which can be wirked with as ones and zeroes. Then we take its mean as the relative proportion of occurences.

mean(z<=13)

mean(z<=8.5)

## Exercise 3: we want to check, whether a two sample t-test rejects or doesn't reject the hypothesis of equality of means
## when generating one sample from N(0,5^2) and the other from N(1,5^2).
## lets try for N=20 and N=1000

N=20
x=rnorm(N,0,5)
y=rnorm(N,1,5)
t.test(x,y)

N=1000
x=rnorm(N,0,5)
y=rnorm(N,1,5)
t.test(x,y)

## We see that for N=20 the test didn't reject the hypothesis and for N=1000 it did.
## For better insight in workings of the t-test we should repeat the simulation many times.

####################
## FUNCTIONS

difference=function(x,y){
 difference=x-y
 return(difference)
}
difference(5,2)
z=difference(5,2)

##
x=rnorm(50)
mean1toN=function(N){
 mean1toN=ifelse(N<=length(x),mean(x[1:N]),mean(x))
 return(mean1toN)
}
mean1toN(6)


####################
## PLOTS

#frequency diagram (histogram)
n=100
x=rnorm(n,100,15)
hist(x)

## history->recording turns on the logging of plots, which can be browsed by PgDn and PgUp

#plot of the dependency of two variables
x=1:20
y=rnorm(20,x,1)
plot(y~x)
plot(y~x,type="l")
points(y~x,pch="+")


# qqnorm - can be used for comparing a sample with the normal distribution
# the points should be distributed along the quadrant axis
x=rexp(20)
y=rnorm(20)
qqnorm(x)
qqline(x)
qqnorm(y)
qqline(y)

## the normality can be tested using the Shapiro-Wilk test (out of scope of the course)
## reject normality if p-value<0.05
shapiro.test(x)
shapiro.test(y)