########################################
## Correlation
########################################
## Ex.lecture
## Heights of fathers
## and heights of their sons
########################################

height_father=c(172,176,180,184,186)
height_son=c(178,183,180,188,190)
   
mean(height_father)
mean(height_son)

var(height_father)
var(height_son)

cov(height_father,height_son)
r=cor(height_father,height_son)
r

plot(height_son~height_father,pch="+")

r/sqrt(1-r^2)*sqrt(3)
qt(0.975,3)

cor.test(height_father,height_son)


########################################
## Linear regression
########################################
## Ex.lecture
## Heights of fathers
## and heights of their sons
########################################

x=height_father;y=height_son

## Plot the data
plot(y~x)

## Estimating the coefficients
mean(x)
mean(y)
var(x)
cov(x,y)
b=cov(x,y)/var(x)
b
a=mean(y)-b*mean(x)
a

## Estimating the coefficients in one call
lm(y~x)

## Draw the estimated regression line 
abline(lm(y~x))

## Prediction of the height of a son, whose father is 175 cm tall
a+b*175

## 95% confidence intervals for the prediction
n=length(x)
x0=175
a+b*x0-qt(0.975,n-2)*summary(lm(y~x))$sigma*sqrt(1/n+(x0-mean(x))^2/var(x)/(n-1))
a+b*x0+qt(0.975,n-2)*summary(lm(y~x))$sigma*sqrt(1/n+(x0-mean(x))^2/var(x)/(n-1))


########################################
## Ex.lecture
## Dependence of the concentration of lactic acid
## in a newbor child's blood
## on the concentration in the mother's blood
########################################

x=c(40,64,34,15,57,45)
y=c(33,46,23,12,56,40)

## Plot the data
plot(y~x)

## Estimating the coefficients
mean(x)
mean(y)
var(x)
cov(x,y)
b=cov(x,y)/var(x)
b
a=mean(y)-b*mean(x)
a

## Estimating the coefficients in one call
lm(y~x)

## Draw the estimated regression line 
abline(lm(y~x))

## Confidence intervals for beta
alpha=0.05;n=length(x)
b-qt(1-alpha/2,df=n-2)*sqrt(sum((y-a-b*x)^2)/(n-2))/sqrt((n-1)*var(x))
b+qt(1-alpha/2,df=n-2)*sqrt(sum((y-a-b*x)^2)/(n-2))/sqrt((n-1)*var(x))

## Coefficient of determination R^2
1-sum((y-a-b*x)^2)/sum((y-mean(y))^2)

## More results from fitting the linear model
summary(lm(y~x))

########################################
## Ex.1
## Weight~height
########################################

x=c(158, 161, 168, 175, 182)
y=c(55, 63, 75, 71, 83)

mean(x)
mean(y)
var(x)
cov(x,y)
b=cov(x,y)/var(x)
b
a=mean(y)-b*mean(x)
a

plot(y~x)
abline(lm(y~x))

lm(y~x)
summary(lm(y~x))

## Confidence intervals for beta
alpha=0.05;n=length(x)
b-qt(1-alpha/2,df=n-2)*sqrt(sum((y-a-b*x)^2)/(n-2))/sqrt((n-1)*var(x))
b+qt(1-alpha/2,df=n-2)*sqrt(sum((y-a-b*x)^2)/(n-2))/sqrt((n-1)*var(x))

## Coefficient of determination R^2
1-sum((y-a-b*x)^2)/sum((y-mean(y))^2)

## More results from the linear model fitting
summary(lm(y~x))

## Prediction of the weight of a person 175cm tall
a+b*175


########################################
## Ex.2
## Distortion of a plastic sheet 
## depending on the used pressure
########################################
x=c(2,4,6,8,10)
y=c(14,35,48,61,80)

mean(x)
mean(y)
var(x)
cov(x,y)
b=cov(x,y)/var(x)
b
a=mean(y)-b*mean(x)

plot(y~x)
abline(lm(y~x))

summary(lm(y~x))

## Fitting the model without beta
summary(lm(y~-1+x))

## Inverse prediction of pressure needed to produce a distortion of 70mm
(70-0.2)/7.9