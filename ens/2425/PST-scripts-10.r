#############
## Ex. lecture - carps
## 
rm(list=ls()) ##remove all allocated objects
n=10
Sn=45.65
Xn=Sn/n
Xn

SX2=208.7
sn2=1/(n-1)*(SX2-n*Xn^2)
sn2

## confidence intervals for the expectation
## two-sided
alpha=0.1
qt(1-alpha/2,df=n-1)
Xn-qt(1-alpha/2,df=n-1)*sqrt(sn2)/sqrt(n)
Xn+qt(1-alpha/2,df=n-1)*sqrt(sn2)/sqrt(n)

## one-sided lower
qt(1-alpha,df=n-1)
Xn+qt(1-alpha,df=n-1)*sqrt(sn2)/sqrt(n)

## confidence intervals for the variance
## two-sided
qchisq(1-alpha/2,df=n-1)
qchisq(alpha/2,df=n-1)
(n-1)*sn2/qchisq(1-alpha/2,df=n-1)
(n-1)*sn2/qchisq(alpha/2,df=n-1)

## one-sided upper
qchisq(1-alpha,df=n-1)
(n-1)*sn2/qchisq(1-alpha,df=n-1)



#############
## Ex.1
n=50
Xn=684.2/n
Xn

sn2=1/49*(18651.3-50*Xn^2)
sn2

alpha=0.01
qt(1-alpha/2,df=n-1)
Xn-qt(1-alpha/2,df=n-1)*sqrt(sn2)/sqrt(n)
Xn+qt(1-alpha/2,df=n-1)*sqrt(sn2)/sqrt(n)

Xn-qt(1-alpha,df=n-1)*sqrt(sn2)/sqrt(n)
Xn+qt(1-alpha,df=n-1)*sqrt(sn2)/sqrt(n)



#############
## Ex.3
n=16
Xn=10.3
sn2=1.2
alpha=0.1

qt(1-alpha/2,df=n-1)
Xn-qt(1-alpha/2,df=n-1)*sqrt(sn2)/sqrt(n)
Xn+qt(1-alpha/2,df=n-1)*sqrt(sn2)/sqrt(n)

qchisq(1-alpha/2,df=n-1)
qchisq(alpha/2,df=n-1)

(n-1)*sn2/qchisq(1-alpha/2,df=n-1)
(n-1)*sn2/qchisq(alpha/2,df=n-1)


#############
## Ex.5
x=c(111, 116, 105, 111, 110, 114, 108, 106, 112, 108, 112, 111, 105, 111, 108, 110)
Xn=mean(x)
Xn

s2=9
s=sqrt(s2)
n=length(x)
n
alpha=0.05
z=qnorm(1-alpha/2)
z

Xn-z*s/sqrt(n)
Xn+z*s/sqrt(n)

z=qnorm(1-alpha)
Xn-z*s/sqrt(n)

hist(x)



#############
## Ex.6
## point estimates
n=400
Sn=240
Xn=Sn/n
Xn

## interval estimates for p
alpha=0.05
z=qnorm(1-alpha/2)
s2=Xn*(1-Xn)
s=sqrt(s2)

Xn-z*s/sqrt(n)
Xn+z*s/sqrt(n)

## finding n for the interval to have length 3
(2*z*s/(0.03))^2