############################
## Hypotheses testing
############################


############################
## Pizza (Ex.3)

x=c(41, 38, 56, 38, 63, 59, 52, 49, 46)
hist(x)
n=length(x)
mean(x)
sd(x)

alpha=0.1
mean(x)-qt(1-alpha/2,df=length(x)-1)*sd(x)/sqrt(length(x))
mean(x)+qt(1-alpha/2,df=length(x)-1)*sd(x)/sqrt(length(x))
t.test(x,mu=45,conf.level=0.90)

alpha=0.05
mean(x)-qt(1-alpha/2,df=n-1)*sd(x)/sqrt(n)
mean(x)+qt(1-alpha/2,df=n-1)*sd(x)/sqrt(n)
t.test(x,mu=45)



############################
## Cars (Ex.6)
n=20
Xn=6.8
sn2=2.56

## One-sided alternative of the type "greater"
## We need the upper 1-alpha interval

alpha=0.05
Xn-qt(1-alpha,df=n-1)*sqrt(sn2)/sqrt(n)

alpha=0.01
Xn-qt(1-alpha,df=n-1)*sqrt(sn2)/sqrt(n)



############################
## Production line (Ex.7)

n=15
sn2=580

## One-sided alternative of the type "greater"
## We need the upper 1-alpha interval

alpha=0.05
(n-1)*sn2/qchisq(1-alpha,df=n-1)

alpha=0.01
(n-1)*sn2/qchisq(1-alpha,df=n-1)



############################
## Beer (Ex.8)

x=c(0.510,0.462,0.491,0.466,0.451,0.503,0.475,0.487,0.512,0.505)
hist(x)
n=length(x)
## average volume:
mean(x)
## estimate of the probability od getting undersized beer:
mean(x<0.5) 

## sample variance:
sqrt(sum((x-mean(x))^2)/(n-1))
sd(x)

alpha=0.05
qt(1-alpha,df=n-1)

## One-sided alternative of the type "less"
## We need the lower 1-alpha interval

alpha=0.05
mean(x)+qt(1-alpha,df=n-1)*sd(x)/sqrt(n)

## Test statisics T:
(mean(x)-0.5)/sd(x)*sqrt(n)

## All-in-one function:
t.test(x,mu=0.5,alternative="less")

## p-value < alpha => reject H0


## level of significance 1% 
alpha=0.01
qt(1-alpha,df=n-1)
mean(x)+qt(1-alpha,df=n-1)*sd(x)/sqrt(n)

t.test(x,mu=0.5,alternative="less",conf.level=0.99)

## test the normality of the data

shapiro.test(x)

## p-value>0.05 => do not reject normality





############################
## TWO SAMPLE PROBLEMS
############################



## PAIRED T-TEST
## used when comparing the expectations of (possibly interdependent) pairs of random variables

height_father=c(172,176,180,184,186)
height_son=c(178,188,177,192,193)
   
t.test(height_son,height_father,paired=T,alternative="greater")

## TWO SAMPLE T-TEST
## used when comparing the expectations between two independent random samples
height_cze=c(169,178,179,186,191)
height_nor=c(175,182,183,189,191,192)

t.test(height_cze,height_nor,paired=F,alternative="two.sided",var.equal=T)

## check whether the samples have equal variance
var.test(height_cze,height_nor)



############################
## Simulated data:

## PAIRED T-TEST
## used when comparing the expectations of (possibly dependent) pairs of random variables
n=20
height_father=rnorm(n,mean=175,sd=5)
height_son=rep(0,n)
for(i in 1:n){height_son[i]   =rnorm(1,mean=height_father[i]+5,sd=5) }

par(mfrow=c(1,2))   ## print two graphs at once
hist(height_father)
hist(height_son)

t.test(height_father,height_son)
t.test(height_father,height_son,alternative="less")

## TWO SAMPLE T-TEST
## used when comparing the expectations between two independent random samples

n=10;m=20
height_usa  =rnorm(m,mean=180,sd=5)
height_china=rnorm(n,mean=165,sd=5)
hist(height_usa)
hist(height_china)


t.test(height_usa,height_china)
t.test(height_usa,height_china,alternative="greater")